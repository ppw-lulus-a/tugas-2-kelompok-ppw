from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from .forms import OrderForm
from .models import Order
from user_laundry.models import userLaundry
from django.views.decorators.http import require_POST


# Create your views here.
def order(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = OrderForm(request.POST)
            print(request.POST)
            if form.is_valid():
                order = form.save()
                order.nama_pengorder = request.user.first_name +" " +request.user.last_name
                order.alamat = request.user.userlaundry.address
                order.nomor_telepon = request.user.userlaundry.number
                order.user_laundry = request.user
                order.save()
                return redirect("order:order")
        else:
            form = OrderForm()
        return render(request, "order/order.html", {"form" : form})
    else:
        return redirect("/")

def calculate_harga(request):
    kategori = request.GET["cat"]
    berat_paket = int(request.GET["brt"])
    jenis_paket = request.GET["pkt"]
    harga_kat = 0
    harga_paket = 0
    if berat_paket <= 0:
        return JsonResponse({"total" : 0})
    harga_total = 0
    if kategori == "diantarkan":
        harga_total += 5000
        harga_kat += 5000
    if jenis_paket == "reguler":
        harga_total += berat_paket * Order.harga_reguler
        harga_paket += Order.harga_reguler
    else:
        harga_total += berat_paket * Order.harga_premium
        harga_paket += Order.harga_premium

    return JsonResponse({"total" : harga_total, "kat": harga_kat, "pkt": harga_paket})

def calculate_total_pengeluaran(request):
    total_pengeluaran = 0
    for order in request.user.order_set.exclude(status = "Ditolak"):
        total_pengeluaran += order.total_harga
    return JsonResponse({"pengeluaran" : total_pengeluaran})

    










