from django.test import TestCase, Client, LiveServerTestCase
from .models import Order
from .views import order
from .forms import OrderForm
from user_laundry.models import userLaundry
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from tugas_ppw_kel_lulus_a.settings import BASE_DIR
import os
import time

# Create your tests here.

class OrderTest(LiveServerTestCase):
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox') 
        #self.browser = webdriver.Chrome(chrome_options=options)
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=options)
        self.client = Client()

        browser = self.browser
        browser.get("%s%s" % (self.live_server_url, "/register/"))
        first_name = browser.find_element_by_id('id_first_name')
        last_name = browser.find_element_by_id('id_last_name')
        username = browser.find_element_by_id('id_username')
        address = browser.find_element_by_id('id_address')
        number = browser.find_element_by_id('id_number')
        password1 = browser.find_element_by_id('id_password1')
        password2 = browser.find_element_by_id('id_password2')

        first_name.send_keys("Bambang")
        last_name.send_keys("Ganteng")
        username.send_keys("Baganteng")
        address.send_keys("bdasjln")
        number.send_keys(346576)
        password1.send_keys("Bacot098")
        password2.send_keys("Bacot098")


        button = browser.find_element_by_id('reg')
        button.click()


    def tearDown(self):
        self.browser.quit()
    
    def test_without_login(self):
        browser = self.browser 
        browser.get("%s%s" % (self.live_server_url, "/order/"))
        time.sleep(1)
        self.assertIn("About Us", browser.page_source)
        self.assertIn("Login", browser.page_source)
    
    def test_calculate_1(self):
        browser = self.browser
        browser.get(self.live_server_url)

        username = browser.find_element_by_id('id_username')
        password = browser.find_element_by_id('id_password')
        username.send_keys("Baganteng")
        password.send_keys("Bacot098")
        button = browser.find_element_by_id('but-login')
        button.click()
        browser.get("%s%s" % (self.live_server_url, "/order/"))
        select_paket = Select(browser.find_element_by_id('id_nama_paket'))
        select_kategori = Select(browser.find_element_by_id('id_category'))
        form_berat = browser.find_element_by_id('id_berat_paket')
        kalkulasi = browser.find_element_by_id('kalkulasi')
        select_kategori.select_by_visible_text("Diantarkan")
        select_paket.select_by_visible_text("Reguler")
        kalkulasi.click()
        
        time.sleep(1)
        self.assertIn("35000", browser.page_source)
    

    def test_calculate_2(self):
        browser = self.browser
        browser.get(self.live_server_url)

        username = browser.find_element_by_id('id_username')
        password = browser.find_element_by_id('id_password')
        username.send_keys("Baganteng")
        password.send_keys("Bacot098")
        button = browser.find_element_by_id('but-login')
        button.click()
        browser.get("%s%s" % (self.live_server_url, "/order/"))
        select_paket = Select(browser.find_element_by_id('id_nama_paket'))
        select_kategori = Select(browser.find_element_by_id('id_category'))
        form_berat = browser.find_element_by_id('id_berat_paket')
        kalkulasi = browser.find_element_by_id('kalkulasi')
        select_kategori.select_by_visible_text("Ambil Sendiri")
        select_paket.select_by_visible_text("Express")
        kalkulasi.click()
        time.sleep(1)
        self.assertIn("45000", browser.page_source)
    
    # def test_calculate_3(self):
    #     browser = self.browser
    #     browser.get(self.live_server_url)

    #     username = browser.find_element_by_id('id_username')
    #     password = browser.find_element_by_id('id_password')
    #     username.send_keys("Baganteng")
    #     password.send_keys("Bacot098")
    #     button = browser.find_element_by_id('but-login')
    #     button.click()

    #     browser.get("%s%s" % (self.live_server_url, "/order/"))
    #     for i in range(2):
    #         select_paket = Select(browser.find_element_by_id('id_nama_paket'))
    #         select_kategori = Select(browser.find_element_by_id('id_category'))
    #         form_berat = browser.find_element_by_id('id_berat_paket')
    #         kalkulasi = browser.find_element_by_id('kalkulasi')
    #         select_kategori.select_by_visible_text("Ambil Sendiri")
    #         select_paket.select_by_visible_text("Express")
    #         kalkulasi.click()
    #         time.sleep(1)
    #         pesan = browser.find_element_by_id('pesan')
    #         pesan.click()

    #         time.sleep(5)
        
    #     self.assertIn("90000", browser.page_source)


        










        

        
