from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User 

# Create your models here.
# class Laundry(models.Model):
#     berat_paket =  models.PositiveIntegerField(default=5)
#     harga_laundry = models.IntegerField(default = 0)

class Order(models.Model):
    harga_reguler = 6000
    harga_premium = 9000
    nama_pengorder = models.CharField(max_length = 20, default = "Jane Doe")
    # nama_paket = models.CharField(max_length = 20, default = "Reguler")
    alamat = models.CharField(max_length = 50, default = "KOS Murah")
    nomor_telepon = models.BigIntegerField(default = "82178150093")
    berat_paket = models.PositiveIntegerField(default = 5)
    total_harga = models.IntegerField(default = 0)
    DROP_CHOICES = (('diantarkan','Diantarkan'),('ambil_sendiri','Ambil Sendiri'))
    category = models.CharField(max_length=20, choices=DROP_CHOICES, default = 'diantarkan')
    DROP_CHOICES1 = (('reguler','Reguler'),('express','Express'))
    nama_paket = models.CharField(max_length=20, choices=DROP_CHOICES1, default = 'reguler')
    status = models.CharField(default = "Menunggu Verifikasi", max_length = 30)
    user_laundry = models.ForeignKey(User, on_delete=models.CASCADE, default = 1)

    








    
    


