from django.urls import path
from . import views

app_name = 'order'
urlpatterns = [
    path('',views.order, name = 'order'),
    path('calculate',views.calculate_harga, name = 'calculate'),
    path('pengeluaran',views.calculate_total_pengeluaran, name = 'calculate'),

]