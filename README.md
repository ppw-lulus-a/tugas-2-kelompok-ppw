# Tugas Kelompok

## Description:
    Dalam tugas kelompok 2 ini, kami menggunakan ide dari tugas kelompok 1. Dimana kami hanya akan menambahkan implementasi dari Java Script 
    dan AJAX serta melakukan styling pada beberapa page yang dibutuhkan. Ide yang sudah kami jelaskan pada Tugas Kelompok 1 sebagai berikut:
    kelompok kami memilih ide mengenai sistem pada laundry, dimana kita tahu bahwa saat ini, banyak kalangan yang menggunakan laundry. 
    sehingga sistem ini dapat mempermudah penyedia jasa laundry. sistem ini memiliki beberapa fitur sebagai berikut:
        1.  pelanggan terlebih dahulu memiliki akun untuk masuk kedalam halaman laundry.
        2.  pelanggan akan diarahkan ke halaman order, dimana di halaman tersbeut pelanggan dapat memilih jenis laundry yang digunakan
            dan jenis pengambilan pesanan (diantar/diambil langsung), setelah itu fitur harga akan muncul secara pop up
        3.  mitra laundry dapat menolak atau menerima pesanan dan pelanggan dapat melihat status pemesanan sudah selesai atau belum
        4.  pelanggan dapat mengisi halaman feedback setelah Pesanan selesai.
    Dalam progress tahap 1 ini kami menggunakan source code yang sama, kami hanya melakukan RED-GREEN pada unit test yang sudah dibuat sebelumnya.
## Member:
- Muhammad Rafif Elfazri (1806205722) [App Order & Laundryadmin]
- Fajar Anugrah Subekti (1806146940) [App Status]
- Rafa Putri Ayasha (1806147136) [App Feeback]
- Ashila Ghassani Astmara (1806205395) [App user_laundry]

## Apps Feature:
- User masuk dengan memasukkan akun yang sudah terdaftar.
- Aplikasi menghitung jumlah biaya laundry yang dikeluarkan sebelum melakukan pemesanan
- User dapat melihat feedback dari pengguna sebelumnya
- Aplikasi dapat mengganti status pemesanan laundry (admin)

## Link Heroku
http://tugas2-kel-ppw-12.herokuapp.com/
