from django.shortcuts import render, redirect
from django.http import JsonResponse
from .models import Feedback
from user_laundry.models import userLaundry
from .forms import *
from django.core import serializers
# from django.views.decorators.http import login_required

# @login_required
def testi(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            pelanggan = request.user.username
            form = CommentForm(request.POST)
            if form.is_valid():
                testi = form.save()
                testi.nama = pelanggan
                testi.save()
            return redirect('feedback:Testimoni')
            pass
        else:
            form = CommentForm()
            return render(request, 'Feedback.html', {'form': form, 'list_testimoni' : Feedback.objects.all()})
    else:
        return redirect("/")

def filter_star(request):
    star = request.GET["star"]
    if star == "all":
        data = serializers.serialize('json', Feedback.objects.all(), fields=("Comment","bintang","nama","date"))
        data = eval(data)
        return JsonResponse({"items": data}, safe= False)
    if star == "1":
        data = serializers.serialize('json', Feedback.objects.filter(bintang = 1), fields=("Comment","bintang","nama","date"))
        data = eval(data)
        return JsonResponse({"items": data}, safe= False)
        
    if star == "2":
        data = serializers.serialize('json', Feedback.objects.filter(bintang = 2), fields=("Comment","bintang","nama","date"))
        data = eval(data)
        return JsonResponse({"items": data}, safe= False)
    if star == "3":
        data = serializers.serialize('json', Feedback.objects.filter(bintang = 3), fields=("Comment","bintang","nama","date"))
        data = eval(data)
        return JsonResponse({"items": data}, safe= False)
    if star == "4":
        data = serializers.serialize('json', Feedback.objects.filter(bintang = 4), fields=("Comment","bintang","nama","date"))
        data = eval(data)
        return JsonResponse({"items": data}, safe= False)
    if star == "5":
        data = serializers.serialize('json', Feedback.objects.filter(bintang = 5), fields=("Comment","bintang","nama","date"))
        data = eval(data)
        return JsonResponse({"items": data}, safe= False)
    

