from django import forms
from .models import Feedback

class CommentForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ['Comment', 'bintang']
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
             })
