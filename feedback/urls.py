
from django.contrib import admin
from django.urls import path
from .views import testi, filter_star

app_name = 'feedback'
urlpatterns = [
    path('',testi, name ='Testimoni'),
    path('filter', filter_star, name = "pilih_star" )
]
