from django.test import TestCase, Client,  LiveServerTestCase
from user_laundry.models import userLaundry
from order.models import Order
from django.contrib.auth.models import User 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from tugas_ppw_kel_lulus_a.settings import BASE_DIR
import os
import time
# Create your tests here.

class CobaTest(TestCase):
    # def setUp(self):
    #     options = Options()
    #     # options.add_argument('--headless')
    #     # options.add_argument('--no-sandbox') 
    #     self.browser = webdriver.Chrome(chrome_options=options)
    #     # self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=options)
    #     self.client = Client()
    
    
    # def tearDown(self):
    #     self.browser.quit()
    

    def test_apakah_ada_slash_status(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code,200)

        
    def test_adakah_judul_pesanan(self):
        response = Client().get('/status/')
        self.assertContains(response, 'Pesanan')
        self.assertEqual(response.status_code,200)

    def test_ada_func_status(self):
        found = resolve('/status/')
        self.assertEqual(found.func, status)

    def test_adakah_button_diproses(self):
        response = Client().get('/status/')
    
    def test_ada_button_diproses(self):
        response = Client().get('/status/')
        self.assertContains(response,'</button>')
    
    def test_diproses_ada_diproses(self):
        response = Client().get('/status/')
        self.assertContains(response, 'Diproses')
    
    def test_diproses_ada_ditolak(self):
        response = Client().get('/status/')
        self.assertContains(response, 'Ditolak')

    
    def test_diproses_ada_selesai(self):
        response = Client().get('/status/')
        self.assertContains(response, 'Selesai')

    
    def test_diproses_ada_menunggu(self):
        response = Client().get('/status/')
        self.assertContains(response, 'Menunggu Verifikasi')


    def test_ada_kata_diproses(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'navbar_user.html')
            

    # def test_adakah_judul_pesanan_diproses(self):
    #     pelanggan = userLaundry(number = 1)
    #     pelanggan.save()
    #     response = Client().get('/status/' + str(pelanggan.id))
    #     self.assertContains(response, 'Pesanan Diproses')
    
    # def test_adakah_judul_pesanan_ditolak(self):
    #     pelanggan = userLaundry(number = 1)
    #     pelanggan.save()
    #     response = Client().get('/status/' + str(pelanggan.id))
    #     self.assertContains(response, 'Ditolak')






        