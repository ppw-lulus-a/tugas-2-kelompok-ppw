from django.shortcuts import render, redirect
from user_laundry.models import userLaundry
from order.models import Order
from django.core import serializers
from django.http import JsonResponse

def status(request):
    return render(request,"status/status.html")

def statusA(request):
    statusOrd = request.GET["statusorder"]
    if statusOrd == "diproses": 
        data = serializers.serialize('json', request.user.order_set.filter(status = "Pesanan Diproses"),fields=("nama_pengorder", "alamat","nomor_telepon","nama_paket",
        "total_harga","status","berat_paket","category"))
        data = eval(data)
        return JsonResponse({"items":data})
    if statusOrd == "ditolak":
        data = serializers.serialize('json', request.user.order_set.filter(status = "Pesanan Ditolak"),fields=("nama_pengorder", "alamat","nomor_telepon","nama_paket",
        "total_harga","status","berat_paket","category"))
        data = eval(data)
        return JsonResponse({"items":data})
    if statusOrd == "selesai":
        data = serializers.serialize('json', request.user.order_set.filter(status = "Pesanan Selesai"),fields=("nama_pengorder", "alamat","nomor_telepon","nama_paket",
        "total_harga","status","berat_paket","category"))
        data = eval(data)
        return JsonResponse({"items":data})
    if statusOrd == "menungguverif":
        print(request.user.order_set.all())
        data = serializers.serialize('json', request.user.order_set.filter(status = "Menunggu Verifikasi"),fields=("nama_pengorder", "alamat","nomor_telepon","nama_paket",
        "total_harga","status","berat_paket","category"))
        data = eval(data)
        return JsonResponse({"items":data})
    