from django.urls import path
from . import views

app_name = 'status'
urlpatterns = [
    path('',views.status, name = 'status'),
    path('filter',views.statusA, name ='filter')
    # path('<id>',views.laundry, name = 'laundry')
    # path('tambahPilihan/',tambahPilihan, name ="tambahPilihan")
]
