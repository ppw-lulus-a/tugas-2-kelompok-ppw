from django.shortcuts import render, redirect
from django.http import JsonResponse
from order.models import Order
from django.core import serializers

# # Create your views here.
def admin(request):
    return render(request, "laundryadmin/admin.html")

def filter_Order(request):
    key = request.GET["key"]
    if key == "Verifikasi":
        filter = Order.objects.filter(status = "Menunggu Verifikasi")
        data = serializers.serialize('json', filter, fields=('nama_pengorder','alamat',
        "nomor_telepon", "berat_paket", "total_harga","category", "nama_paket", "status"))
        return JsonResponse({"items" : eval(data)})
    elif key == "Selesai":
        filter = Order.objects.filter(status = "Pesanan Selesai")
        data = serializers.serialize('json', filter, fields=('nama_pengorder','alamat',
        "nomor_telepon", "berat_paket", "total_harga","category", "nama_paket", "status"))
        return JsonResponse({"items" : eval(data)})
    elif key == "Diproses":
        filter = Order.objects.filter(status = "Pesanan Diproses")
        data = serializers.serialize('json', filter, fields=('nama_pengorder','alamat',
        "nomor_telepon", "berat_paket", "total_harga","category", "nama_paket", "status"))
        return JsonResponse({"items" : eval(data)})
    else:
        filter = Order.objects.filter(status = "Ditolak")
        data = serializers.serialize('json', filter, fields=('nama_pengorder','alamat',
        "nomor_telepon", "berat_paket", "total_harga","category", "nama_paket", "status"))
        return JsonResponse({"items" : eval(data)})




def accept_order(request):
    id = request.GET["id"]
    order = Order.objects.get(pk = id)
    order.status = "Pesanan Diproses"
    order.save()
    return JsonResponse({"True": True})

def finish_order(request):
    id = request.GET["id"]
    order = Order.objects.get(pk = id)
    order.status = "Pesanan Selesai"
    order.save()
    return JsonResponse({"True": True})

def cancel_order(request):
    id = request.GET["id"]
    order = Order.objects.get(pk = id)
    order.status = "Ditolak"
    order.save()
    return JsonResponse({"True": True})

def total_pendapatan(request):
    filter = Order.objects.filter(status = "Pesanan Selesai")
    x = 0
    for order in filter:
        x += order.total_harga
    return JsonResponse({"income": x})