from django.test import TestCase, Client, LiveServerTestCase
from order.models import Order
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.auth.models import User 
from tugas_ppw_kel_lulus_a.settings import BASE_DIR
import os
import time

# Create your tests here.
class LaundryAdminTest(LiveServerTestCase):
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox') 
        #self.browser = webdriver.Chrome(chrome_options=options)
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=options)
        self.client = Client()

        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.id = []
        for i in range(10):
            order = Order(total_harga = 50000, nama_paket = "express", category = "diantarkan", berat_paket = 5, user_laundry = user)
            order.save()
            self.id.append(order.id)
    
    def tearDown(self):
        self.browser.quit()
        
    def test_url_exist(self):
        c = Client()
        response = c.get('/laundryadmin/')
        self.assertEqual(response.status_code, 200)

    
    def test_filter_Order(self):
        browser = self.browser
        browser.get(self.live_server_url)

        browser.get("%s%s" % (self.live_server_url, "/laundryadmin/"))
        time.sleep(1)
        select_filter = Select(browser.find_element_by_id('select_filter'))

        select_filter.select_by_visible_text("Menunggu Verifikasi")
        time.sleep(1)
        self.assertIn("50000", browser.page_source)

        select_filter.select_by_visible_text("Pesanan Diproses")
        time.sleep(1)
        self.assertNotIn("50000", browser.page_source)

        select_filter.select_by_visible_text("Pesanan Ditolak")
        time.sleep(1)
        self.assertNotIn("50000", browser.page_source)

        select_filter.select_by_visible_text("Pesanan Selesai")
        time.sleep(1)
        self.assertNotIn("50000", browser.page_source)
    
    def test_accept(self):
        c = self.client 
        c.get("/laundryadmin/terima?id=" + str(self.id[0]))
        response = c.get("/laundryadmin/filter?key=Diproses")
        self.assertIn("50000", response.content.decode())
    
    def test_finish(self):
        c = self.client 
        c.get("/laundryadmin/terima?id=" + str(self.id[0]))
        c.get("/laundryadmin/selesai?id=" + str(self.id[0]))
        response = c.get("/laundryadmin/filter?key=Selesai")
        self.assertIn("50000", response.content.decode())
        response = c.get("/laundryadmin/income")
        self.assertIn("50000", response.content.decode())
    
    def test_cancel(self):
        c = self.client 
        c.get("/laundryadmin/tolak?id=" + str(self.id[0]))
        response = c.get("/laundryadmin/filter?key=Ditolak")
        self.assertIn("50000", response.content.decode())



    
    


