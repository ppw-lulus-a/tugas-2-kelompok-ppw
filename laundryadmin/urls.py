from django.urls import path
from . import views

app_name = 'laundryadmin'
urlpatterns = [
    path('',views.admin, name = 'admin'),
    path('filter', views.filter_Order, name='filter'),
    path('terima',views.accept_order, name = 'accept'),
    path('selesai', views.finish_order, name = 'finish'),
    path('tolak', views.cancel_order, name = 'cancel'),
    path('income', views.total_pendapatan, name = 'income'),
]