from django.db import models
from django.contrib.auth.models import User

# Create your models here.

#  This will auto create a profile of user with blank phone number that can be updated later.

class userLaundry(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default = 1)
    address = models.CharField(max_length=50, default = 'Jalan Duren')
    number = models.BigIntegerField(default = 298721)

#     @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         userLaundry.objects.create(user=instance)