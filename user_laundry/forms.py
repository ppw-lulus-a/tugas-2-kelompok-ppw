from django.db import models
from django import forms
from .models import userLaundry
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class registerForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Please enter your name')
    last_name = forms.CharField(max_length=30, required=False, help_text='Please enter your name')
    address = forms.CharField(max_length=100, required=False, help_text='Please enter your address')
    number = forms.IntegerField(required=False, help_text='Please enter your number')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username','address','number', 'password1', 'password2', )

