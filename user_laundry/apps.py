from django.apps import AppConfig


class UserLaundryConfig(AppConfig):
    name = 'user_laundry'
