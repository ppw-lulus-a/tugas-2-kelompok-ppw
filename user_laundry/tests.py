from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from tugas_ppw_kel_lulus_a.settings import BASE_DIR
import os
import time

# Create your tests here.

class user_laundry_unit_Test(LiveServerTestCase):
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox') 
        #self.browser = webdriver.Chrome(chrome_options=options)
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=options)
        self.client = Client()

        browser = self.browser
        browser.get("%s%s" % (self.live_server_url, "/register/"))
        first_name = browser.find_element_by_id('id_first_name')
        last_name = browser.find_element_by_id('id_last_name')
        username = browser.find_element_by_id('id_username')
        address = browser.find_element_by_id('id_address')
        number = browser.find_element_by_id('id_number')
        password1 = browser.find_element_by_id('id_password1')
        password2 = browser.find_element_by_id('id_password2')

        first_name.send_keys("Bambang")
        last_name.send_keys("Ganteng")
        username.send_keys("Baganteng")
        address.send_keys("bdasjln")
        number.send_keys(346576)
        password1.send_keys("Bacot098")
        password2.send_keys("Bacot098")


        button = browser.find_element_by_id('reg')
        button.click()

        time.sleep(10)

    

    def test_apakah_dataPelanggan_app_url_ada(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_apakah_dataPelanggan_app_menggunakan_fungsi_homepage(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)
    
    def test_apakah_dataPelanggan_app_menggunakan_fungsi_register(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_apakah_dataPelanggan_app_menggunakan_fungsi_show(self):
        found = resolve('/show/')
        self.assertEqual(found.func, show)
    
    def test_apakah_terdapat_tulisan_MyLaundry_di_Homepage_html(self):
        request = HttpRequest()
        response = homepage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('MyLaundry', html_response)

    def test_apakah_terdapat_tulisan_AboutUs_di_Homepage_html(self):
        request = HttpRequest()
        response = homepage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('About Us', html_response)

    def test_fungsi_login(self):
        browser = self.browser
        browser.get(self.live_server_url)
        username = browser.find_element_by_id('id_username')
        username.send_keys("Baganteng")
        time.sleep(5)
        password = browser.find_element_by_id('id_password')
        password.send_keys("Bacot098")
        time.sleep(5)

        submit = browser.find_element_by_id('but-login')
        submit.send_keys(Keys.RETURN)
        time.sleep(5)

        self.assertIn('Baganteng', browser.page_source)
        time.sleep(10)
    
    def test_fungsi_login_gagal(self):
        browser = self.browser
        browser.get(self.live_server_url)
        username = browser.find_element_by_id('id_username')
        username.send_keys("coba")
        password = browser.find_element_by_id('id_password')
        password.send_keys("cobacoba")

        submit = browser.find_element_by_id('but-login')
        submit.send_keys(Keys.RETURN)
        time.sleep(5)

        self.assertIn('Login', browser.page_source)
        time.sleep(5)
        
    def test_fungsi_logout(self):
        browser = self.browser
        browser.get(self.live_server_url)
        browser = self.browser
        browser.get(self.live_server_url)
        
        username = browser.find_element_by_id('id_username')
        username.send_keys("Baganteng")
        time.sleep(5)
        password = browser.find_element_by_id('id_password')
        password.send_keys("Bacot098")
        time.sleep(5)

        submit = browser.find_element_by_id('but-login')
        submit.send_keys(Keys.RETURN)
        time.sleep(5)
        
        browser.find_element_by_id('click-logout').click()
        time.sleep(5)
    
    def tearDown(self):
        self.browser.quit()

    








