from django.urls import path
from . import views 

#url for app

app_name = 'user_laundry'

urlpatterns = [
    path('',views.homepage, name='homepage'),
    path('register/', views.register, name='register'),
    path('show/',views.show,name='show'),
    path('',views.logout,name='logout'),
]