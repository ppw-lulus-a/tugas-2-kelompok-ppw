from django.shortcuts import render, redirect
from django.http import JsonResponse
from .models import userLaundry
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login
from .forms import registerForm
import requests
import json

# Create your views here.

def homepage(request):
    if request.method == "POST":
        form = AuthenticationForm(data = request.POST)
        if form.is_valid():
            user = form.get_user()
            print(user)
            login(request, user)
            return redirect('user_laundry:show')
    else:
        form = AuthenticationForm()
    return render(request, "Homepage.html", {"form" : form })


def register(request):
    if request.method == 'POST':
        form_regis = registerForm(request.POST)
        if form_regis.is_valid():
            x = form_regis.save(commit = False)
            x.first_name = form_regis.cleaned_data.get('first_name')
            x.last_name = form_regis.cleaned_data.get('last_name')
            x.save()

            address1 = form_regis.cleaned_data.get('address')
            number1 = form_regis.cleaned_data.get('number')
            user_laundry = userLaundry(address = address1, number = number1)
            user_laundry.user = x
            user_laundry.save()

            x.save()
            print(x.userlaundry)
            return redirect('user_laundry:homepage')
    else:
        form_regis = registerForm()
    return render(request, "register.html", {'form': form_regis})


def show(request):
    username_login = request.user.username
    return render(request,'Show.html',{'name' : username_login, "navbar" : True})

def logout(request):
    return render(request,'Homepage.html')

