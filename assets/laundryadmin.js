$(document).ready(()=>{
    income()
    laundry_ajax();

    $("#select_filter").change(()=>{
        $("#kotak").empty();
        laundry_ajax();
        income();
    })
})


function laundry_ajax(){
    let key = $("#select_filter").val();
    $("#kotak").empty();
    $.ajax({
        method: 'GET',
        url: "/laundryadmin/filter?key=" + key,
        success: function(response){
            for (i = 0; i < response.items.length; i++){
            
            let hasil = response.items[i]
            let fields = hasil.fields

            let html = 
            `<div class="status">
            <p id =`+ "nama"+i +`>Nama:</p>
            <p id =`+ "alamat"+i +`>Alamat:</p>
            <p id =`+ "nomor"+i +`>Nomor Telepon:</p>
            <p id =`+ "paket"+i +`>Paket:</p>
            <p id =`+ "antar"+i +`>Pengantaran: Pickup</p>
            <p id =`+ "berat"+i +`>Berat :Kg</p>
            <p id =`+ "total"+i +`>Total Harga: Rp</p>
            <p id =`+ "stats"+i +`>Status:</p>
            <div id = `+ "kotak-tombol"+i +` class = "mb-1" style="display: flex; justify-content: space-evenly;"> 
                </div>
            </div>`;
 
            $("#kotak").append(html);
            $("#nama"+i).html("Nama:" +fields.nama_pengorder);
            $("#alamat"+i).html("Alamat: " + fields.alamat);
            $("#nomor"+i).html("Nomor: " + fields.nomor_telepon);
            $("#paket"+i).html("Paket: " +fields.nama_paket);
            $("#antar"+i).html("Pengantaran: " +fields.category);
            $("#berat"+i).html("Berat: " +fields.berat_paket+"Kg");
            $("#total"+i).html("Total Harga: Rp" + fields.total_harga);
            $("#stats"+i).html("Status: " + fields.status);

            let id_order = hasil.pk
            console.log(hasil.pk)
            if(key == "Verifikasi"){
                let button_accept = '<button id = AC'  + i + ' value="Accept">Accept</button>';
                let button_tolak = '<button id = DC'  + i + ' value="Decline">Decline</button>';
                $("#kotak-tombol" + i).append(button_accept);
                $("#kotak-tombol" + i).append(button_tolak);
                $("#AC" + i).click(() =>{
                    $.ajax({
                        method: "GET",
                        url: "/laundryadmin/terima?id=" + id_order,
                        success: function(response){
                            if (response.True == true){
                                laundry_ajax()
                            }    
                        }
                    })
                })
                $("#DC" + i).click(() =>{
                    $.ajax({
                        method: "GET",
                        url: "/laundryadmin/tolak?id=" + id_order,
                        success: function(response){
                            if (response.True == true){
                                laundry_ajax()
                            }    
                        }
                    })
                })
            }
            else if (key == "Diproses"){
                let button_finish = '<button id =' + "FN" + i +' value="Finish">Finish</button>';
                $("#kotak-tombol" + i).append(button_finish);
                $("#FN" + i).click(() =>{
                    $.ajax({
                        method: "GET",
                        url: "/laundryadmin/selesai?id=" + id_order,
                        success: function(response){
                            if (response.True == true){
                                laundry_ajax()
                            }    
                        }
                    })
                })
                
                }
            }  
        }
    })
}

function income(){
    $.ajax({
        method: "GET",
        url: "/laundryadmin/income",
        success: function(response){
            $("#income").html("Rp" + response.income);
        }
    })
}