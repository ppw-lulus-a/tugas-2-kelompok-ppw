$(document).ready(()=>{
    $("#hasil").hide();
    $("#div_id_total_harga").hide();
    calculate_total();
    $("#kalkulasi").click(() =>{
        let paket = $("#id_nama_paket").val();
        let berat = $("#id_berat_paket").val();
        let category = $("#id_category").val();
        $("#hasil").show();
        if ($("#pesan").length == 0){
            $.ajax({
                method: 'GET',
                url: '/order/calculate?cat=' + category + '&brt=' + berat + '&pkt=' + paket,
                success: function(response){
                    let html = 
                    `<table class="table table-borderless">
                    <tbody>
                    <tr class="">
                        <td colspan="3"><h2 class="text-center mr-5">Kalkulasi Harga</h2></td>
                    </tr>
                    <tr class="">
                        <td><h4>Pengantaran</h4></td>
                        <td class=""><h4>:</h4></td>
                        <td><h4 id = "antar">Rpaljscn</h4></td>
                    </tr>
                    <tr class="">
                        <td><h4>Biaya(/Kg)</h4></td>
                        <td class="col-1"><h4>:</h4></td>
                        <td><h4 id ="biaya">Rpbasdjlnzx</h4></td>
                    </tr>
                    <tr class="border-top border-dark" style="border-width:5px !important;">
                        <td><h4>Total</h4></td>
                        <td class=""><h4>:</h4></td>
                        <td><h4 id = "total" >Rp</h4></td>
                    </tr>
                    </tbody>
                    </table>
                    <div class="d-flex justify-content-center">
                            <button id = "pesan" class="btn btn-primary" type="submit">Pesan</button>
                    </div>`;
                    $("#hasil").append(html)
                    $("#antar").html("Rp" + response.kat)
                    $("#biaya").html("Rp" + response.pkt + " x " + berat)
                    $("#total").html("Rp" + response.total)   
                    $("#pesan").mouseover( () => {
                        $("#pesan").removeClass("btn-primary")
                        $("#pesan").addClass("btn-danger")
                    })
                    $("#pesan").mouseleave( () => {
                        $("#pesan").removeClass("btn-danger")
                        $("#pesan").addClass("btn-primary")
                    })
                    $("#id_total_harga").val(response.total);
                }
            })
        }
        else{
            $.ajax({
                method: 'GET',
                url: '/order/calculate?cat=' + category + '&brt=' + berat + '&pkt=' + paket,
                success: function(response){
                    $("#antar").html("Rp" + response.kat)
                    $("#biaya").html("Rp" + response.pkt + " x " + berat)
                    $("#total").html("Rp" + response.total)   
                }
            })
        }          
    })

    $("#id_nama_paket, #id_berat_paket, #id_category").change(() => {
        let berat = $("#id_berat_paket").val();
        if (berat <= 0){
            $("#id_berat_paket").val(1);
            berat = 1;
        }
        if ($("#pesan").length > 0){
            let paket = $("#id_nama_paket").val();
            let category = $("#id_category").val();
            $.ajax({
                method: 'GET',
                url: '/order/calculate?cat=' + category + '&brt=' + berat + '&pkt=' + paket,
                success: function(response){
                    $("#antar").html("Rp" + response.kat)
                    $("#biaya").html("Rp" + response.pkt + " x " + berat)
                    $("#total").html("Rp" + response.total)   
                }
            })

        }

    })
    $("#kalkulasi").mouseover( () => {
        $("#kalkulasi").removeClass("btn-primary")
        $("#kalkulasi").addClass("btn-danger")
    })
    $("#kalkulasi").mouseleave( () => {
        $("#kalkulasi").removeClass("btn-danger")
        $("#kalkulasi").addClass("btn-primary")
    })

})

function calculate_total(){
    let pengeluaran = $("#pengeluaran")
    $.ajax({
        method: 'GET',
        url: "/order/pengeluaran",
        success: function(response){
            $(pengeluaran).html("Rp" + response.pengeluaran)
        }
    })
}




